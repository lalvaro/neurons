package com.betomorrow.dojos.neurons;

/**
 * Created by lalvaro on 17/10/2016.
 */
public interface OCR {
    void    learn(Sample[] samples);
    float   check(Sample input);
}
