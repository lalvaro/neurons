package com.betomorrow.dojos.neurons;

import java.util.Random;

/**
 * Created by lalvaro on 20/10/2016.
 */
public class NeuronalOCR implements OCR {

    class Unit {
        float in;
        float out;
        float gradient;
        float weights[];

        public Unit() {
            weights = new float[Sample.SAMPLE_SIZE];
        }
    }

    private Unit[] pixels = new Unit[Sample.SAMPLE_SIZE];
    private Unit[] layer1 = new Unit[Sample.SAMPLE_SIZE];
    private Unit[] layer2 = new Unit[Sample.SAMPLE_SIZE];
    private Unit[] output = new Unit[1];

    public NeuronalOCR() {
        for(int i=0; i<Sample.SAMPLE_SIZE; i++) {
            pixels[i] = new Unit();
            layer1[i] = new Unit();
            layer2[i] = new Unit();
        }
        output[0] = new Unit();
    }

    private float sigma(float value) {
        return 1.f / (1.f + (float) Math.exp(-value));
    }
    private float sigmaDerivative(float value) {
        return sigma(value)*(1-sigma(value));
    }

    public float check(Sample input) {
        return 0.5f;
    }

    public void learn(Sample[] samples) {
    }
}
