package com.betomorrow.dojos.neurons;

/**
 * Created by lalvaro on 03/11/2016.
 */
public enum Sample {

    CHAR_OK1 (new float[] {
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0
    }, 1.f),

    CHAR_OK2 (new float[] {
            0, 0, 1, 0, 0,
            0, 1, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0
    }, 1.f),

    CHAR_OK3 (new float[] {
            0, 1, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0
    }, 1.f),

    CHAR_OK4 (new float[] {
            0, 0, 1, 0, 0,
            0, 1, 1, 0, 0,
            1, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 1, 1, 1, 0
    }, 1.f),

    CHAR_OK5 (new float[] {
            0, 0, 1, 0, 0,
            0, 1, 1, 0, 0,
            1, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0,
            0, 0, 1, 0, 0
    }, 1.f),

    CHAR_NOK1 (new float[] {
            0, 0, 1, 0, 1,
            0, 0, 1, 0, 1,
            0, 0, 1, 0, 1,
            0, 0, 1, 1, 1,
            0, 0, 1, 0, 1,
            0, 0, 1, 0, 1
    }, 0.f),

    CHAR_NOK2 (new float[] {
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0,
            0, 0, 0, 0, 0
    }, 0.f),

    CHAR_NOK3 (new float[] {
            0, 1, 1, 1, 0,
            0, 0, 0, 1, 0,
            0, 0, 0, 1, 0,
            0, 1, 1, 1, 0,
            0, 1, 0, 0, 0,
            0, 1, 1, 1, 0
    }, 0.f),

    CHAR_NOK4 (new float[] {
            0, 1, 1, 1, 0,
            0, 0, 0, 1, 0,
            0, 1, 1, 1, 0,
            0, 0, 0, 1, 0,
            0, 0, 0, 1, 0,
            0, 1, 1, 1, 0
    }, 0.f),

    CHAR_NOK5 (new float[] {
            0, 1, 0, 1, 0,
            0, 1, 0, 1, 0,
            0, 1, 1, 1, 0,
            0, 0, 0, 1, 0,
            0, 0, 0, 1, 0,
            0, 0, 0, 1, 0
    }, 0.f),

    CHAR_NOK6 (new float[] {
            0, 1, 1, 1, 0,
            0, 1, 0, 0, 0,
            0, 1, 0, 1, 0,
            0, 1, 1, 1, 0,
            0, 1, 0, 1, 0,
            0, 1, 1, 1, 0
    }, 0.f);

    private float[] data;
    private float expectedValue;

    public float[]  getData() {
        return data;
    }
    public float    getExpectedValue() {
        return expectedValue;
    }

    Sample(float[] data, float validity) {
        this.data = data;
        this.expectedValue = validity;
    }

    public static final int SAMPLE_SIZE = 30;
}