package com.betomorrow.dojos.neurons;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lalvaro on 03/11/2016.
 */
public class NeuronalOCRTest {

    private NeuronalOCR ocr;

    private void baseTest(Sample[] samples) {
        ocr = new NeuronalOCR();
        ocr.learn(samples);

        for(Sample s : samples) {
            Boolean isValid = s.getExpectedValue() == 1.f;
            assertEquals(isValid, ocr.check(s)>0.5f);
        }
    }

    @Test
    public void testLearnOneValidSampleAndShouldAcceptIt() {
        baseTest(new Sample[] { Sample.CHAR_OK1 });
    }

    @Test
    public void testLearnOneInvalidSampleAndShouldRejectIt() {
        baseTest(new Sample[] { Sample.CHAR_NOK1 });
    }

    @Test
    public void testLearnHalfOfSamplesAndPerformRandomChecks() {
        baseTest(new Sample[] { Sample.CHAR_OK1, Sample.CHAR_OK2, Sample.CHAR_NOK1, Sample.CHAR_NOK2 });
    }


    @Test
    public void testLearnAllSamplesAndPerformRandomChecks() {
        baseTest(new Sample[] {Sample.CHAR_OK1, Sample.CHAR_OK2, Sample.CHAR_OK3, Sample.CHAR_OK4, Sample.CHAR_OK5,
                Sample.CHAR_NOK1, Sample.CHAR_NOK2, Sample.CHAR_NOK3, Sample.CHAR_NOK4, Sample.CHAR_NOK5, Sample.CHAR_NOK6});
    }
}